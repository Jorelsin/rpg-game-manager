import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.util.ArrayList;
import java.awt.event.*;
import java.util.List;


public class GameFrame {

    String path;
    JFrame frame;
    JTextArea loot = new JTextArea();
    JsonReader reader;
    JButton reset = new JButton();
    JSlider Gold;
    JSlider Stones;
    JSlider Gear;
    JSlider Consumables;
    JSlider Size;

    GameFrame(String name, String sysDir) {

        this.frame = new JFrame(name);
        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.path = new ProjectPicker().getChooser(frame);
        this.setSliders();
        reader = new JsonReader(sysDir);
        setGameFrame(reader.getAppSize(path));
    }

    public void setSliders(){
        Gold = new JSlider(0,10,4);
        Stones = new JSlider(0,10,2);
        Gear = new JSlider(0,10,2);
        Consumables = new JSlider(0,10,2);
        Size = new JSlider(0,30,20);
    }

    public void setGameFrame(int size[]){

        frame.setSize(size[0],size[1]);
        frame.add(setPanels());
        frame.setBackground(Color.DARK_GRAY);
        frame.setVisible(true);

    }

    public JLabel getCampaingName() {
        JLabel Name = new JLabel(reader.getCName(path));
        Name.setBounds(2, 0, 125, 30);
        Name.setFont(new Font("Arial", Font.BOLD,12));
        Name.setForeground(Color.BLACK);
        return Name;
    }

    private String getLootTable(String table, int amount){
        String loot = "";

        int diff = amount - (Gold.getValue() + Stones.getValue() +Gear.getValue() +Consumables.getValue());
        int[] totals = {Gold.getValue(),Stones.getValue(),Gear.getValue(),Consumables.getValue(),0};
        if(diff >= 0){
            totals[4] = diff;

        }else{
            diff = diff*-1;
            diff = diff/4;
            for(int i = 0 ; i < totals.length; i++){
                totals[i] = totals[i] - diff ;
            }
            totals[4]= 0;
        }

        loot = loot + reader.getLoot(table, totals)+System.lineSeparator();

        return loot;
    }

    public JPanel setPanels(){

        JPanel MainPangel = new JPanel();
        MainPangel.setLayout(new BoxLayout(MainPangel, BoxLayout.LINE_AXIS));
        JPanel ButtonPanel = new JPanel();
        ButtonPanel.setLayout(new BoxLayout(ButtonPanel, BoxLayout.PAGE_AXIS));
        JPanel Cname = new JPanel();
        JPanel LootTable = new JPanel();
        // adds a button that can be used for making a new table
        reset.setText("RESET TABLE");
        reset.setLocation(50,300);
        reset.setSize(60,20);



        reset.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                loot.setText(getLootTable("LootA", Size.getValue()));
            }
        });

        //TODO: Add a text field where the user can specify which table they wanna use, like A, B or C


        // Panel for the name of the Campaing/file
        Cname.setBounds(0,0 , 125, 30);
        Cname.add(getCampaingName());

        // Makes a random list of loot

        loot.setText(getLootTable("LootA", Size.getValue()));
        loot.setEditable(false);

        ButtonPanel.add(Size);
        ButtonPanel.add(Gold);
        ButtonPanel.add(Gear);
        ButtonPanel.add(Consumables);
        ButtonPanel.add(Stones);
        ButtonPanel.add(reset);
        LootTable.add(loot);
        MainPangel.add(Cname);
        MainPangel.add(ButtonPanel);
        MainPangel.add(LootTable);
        return  MainPangel;
    }

}
