public class Enums {

    public enum Materials{
        Golden, Silver, Bronze, Copper, Mithril, Jade, Bone, Obsidian, Brass, Iron, Steel
    }

    public enum JewelryType{
        Tiara, Ring, Necklace, Bracelets, Earrings, Broche, Amulet, Circlet, Crown
    }

    public enum SimpleMeleeWeapons{
        Dagger, Spear, Handaxe, Lightmace
    }

    public enum SimpleRangedWeapons{
        Shortbow, Crossbow, Javelin
    }

    public enum MartialMeleeWeapons{
        Longsword, Battleaxe, Warhammer, Greatsword, Glaive, Maul, Greataxe, Trident, Shortsword
    }

    public enum MartialRangedWeapons{
        Longbow
    }

    public enum LightArmors{
        Leather, Studded
    }

    public enum MediumArmors{
        Hide, Scalemail
    }

    public enum  HeavyArmors{
        Chainmail, Platemail
    }

    public enum Shields{
        Shield, TowerShield, Buckler
    }
}
