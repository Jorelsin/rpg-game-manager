/*
RPG-Manager v0.01
Author: Hakim El Amri

  */
import javax.swing.*;
import java.io.IOException;

public class MainWindow {

    public static void main(String[] args) throws IOException, ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {

        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        String appName = "Jorelsin´s Game Manager";
        GameFrame mainwindow = new GameFrame(appName, System.getProperty("user.dir"));

    }
}
