
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class JsonReader {

    String lootDir;
    Random rand;

    public JsonReader(String sysDir) {
        lootDir = sysDir + "\\Tables\\Loot.json";
        rand = new Random();
    }

    // get the name of the campaign from the json file
    public String getCName(String Cname){
        String jsonText;
        JSONObject reader;
        String name = null;
        try{
            jsonText = org.apache.commons.io.IOUtils.toString(new FileInputStream(new File(Cname)));
            reader = new JSONObject(jsonText.trim());
            name = reader.getJSONObject("CampaingInfo").getString("Name");
            return name;

        }catch (IOException e){
            e.printStackTrace();
        }
        if (name == null){
            name = "ERROR!";
        }
        return name;
    }

    //gets the settings of the campaign/file/project so the app is opened in the prefferd size and colors*
    //*Soon
    public int[] getAppSize(String Cname){
        String jsonText;
        JSONObject parser;
        int size[] = new int[2];
        try{
            jsonText = org.apache.commons.io.IOUtils.toString(new FileInputStream(new File(Cname)));
            parser = new JSONObject(jsonText.trim());
            size[0] =  parser.getJSONObject("AppInfo").getInt("appW");
            size[1] =  parser.getJSONObject("AppInfo").getInt("appH");


        }catch (IOException e){
            e.printStackTrace();
        }
        return size;
    }

    public String getLoot(String table, int[] amounts){

        String jsonText;
        JSONObject parser;
        ArrayList<String> lootTable = new ArrayList<>();
        String loot ="";

        try{
            jsonText = org.apache.commons.io.IOUtils.toString(new FileInputStream((new File(lootDir))));
            parser = new JSONObject(jsonText.trim()).getJSONObject(table);

            for(int i = 0; i < parser.getJSONArray("Loot").length(); i ++ ){
                    for(int j = 0; j < parser.getJSONArray("Weights").getInt(i); j++){
                        lootTable.add(parser.getJSONArray("Loot").getString(i));
                    }
            }

        }catch (IOException e){
            e.printStackTrace();
        }

        int n = rand.nextInt(lootTable.size());


        /*

        switch (lootTable.get(n)){
            case "Gold" :
                if(table.equals("TableA")){
                    int coins = rand.nextInt(4)+rand.nextInt(4)+rand.nextInt(4)+3;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d4)";
                }else if(table.equals("TableB")){
                    int coins = rand.nextInt(6)+rand.nextInt(6)+rand.nextInt(6)+3;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d6)";
                }else{
                    int coins = rand.nextInt(10)+rand.nextInt(10)+rand.nextInt(10)+3;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d10)";
                }
                break;
            case "Silver" :
                if(table.equals("TableA")){
                    int coins = rand.nextInt(6)+rand.nextInt(6)+rand.nextInt(6)+3;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d6)";
                }else if(table.equals("TableB")){
                    int coins = rand.nextInt(10)+rand.nextInt(10)+rand.nextInt(10)+3;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d10)";
                }else{
                    int coins = rand.nextInt(20)+rand.nextInt(20)+rand.nextInt(20)+3;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d20)";
                }
                break;
            case "Copper":
                if(table.equals("TableA")){
                    int coins = rand.nextInt(10)+rand.nextInt(10)+rand.nextInt(10)+3;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d10)";
                }else if(table.equals("TableB")){
                    int coins = rand.nextInt(20)+rand.nextInt(20)+rand.nextInt(20)+33;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d20+30)";
                }else{
                    int coins = rand.nextInt(20)+rand.nextInt(20)+rand.nextInt(20)+93;
                    loot = coins + " "+lootTable.get(n) +" Coins (3d20+90)";
                }
                break;
            case "Precious Stones":
                int rocks = rand.nextInt(4)+1;
                loot = rocks + " "+lootTable.get(n);
                break;
            case "Jewelery":
                String material = Enums.Materials.values()[rand.nextInt(Enums.Materials.values().length)].toString();
                String type = Enums.JewelryType.values()[rand.nextInt(Enums.JewelryType.values().length)].toString();
                loot = lootTable.get(n)+" - " + material+ " " + type;
                break;
            case "Potion" :
                loot = lootTable.get(n);
                break;
            case "Simple Melee Weapon" :
                loot = "A " + Enums.SimpleMeleeWeapons.values()[rand.nextInt(Enums.SimpleMeleeWeapons.values().length)].toString();
                break;
                default:
                    loot = lootTable.get(n);
                    break;

        }

        */

        return loot;
    }
}
