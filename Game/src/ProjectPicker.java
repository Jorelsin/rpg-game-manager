import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ProjectPicker {


    JFileChooser fileChooser = fileChooser = new JFileChooser("Campaigns");;

    public String getChooser(JFrame parent){
        String json ="";
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Campaings", "json");
        fileChooser.setFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(parent);
        if(returnVal == JFileChooser.APPROVE_OPTION){
            return json = fileChooser.getSelectedFile().getPath();
        }
        if(json == ""){
            System.out.println("You decided not to pick a Campaign");
            System.exit(1);
        }
        return json;
    }
}
