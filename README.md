# RPG Game Manager

A desktop tool for the aspiring GM, to help keep track of there game session while they are playing.

# MainWindow
The main entry point of the program.

# GameFrame
The part of the program that handles the current session of the user.

# JsonReader
Reads the json document of the campaign.

# JsonWriter
Writes over the json document of the campaign.
